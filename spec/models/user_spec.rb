require 'rails_helper'

RSpec.describe User, type: :model do
  let!(:user) { create(:user) }

  describe 'validations' do
    it "is valid with valid attributes" do
      expect(user).to be_valid
    end

    it "should require a unique email" do
      expect { 
        build(:user, email: user.email).save!
    }.to raise_error(/Email has already been taken/)
    end

    it "should not allow email without @" do
      expect {
        build(:user, email: "hello").save!
    }.to raise_error(/Email is invalid/)
    end
  end
end
