require 'ffaker'

FactoryBot.define do
  factory :category do
    name    FFaker::Lorem.word
    image   FFaker::Avatar.image
  end
end