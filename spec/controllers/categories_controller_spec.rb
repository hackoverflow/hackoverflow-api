require 'rails_helper'

RSpec.describe CategoriesController, type: :controller do
  let!(:categories) { create_list(:category, 5) }
  describe 'index' do
    it "fetches all categories" do
      expect(categories.length).to eq(5)
    end
  end
end
