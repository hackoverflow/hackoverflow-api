Rails.application.routes.draw do
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  devise_for :users
  
  post '/api/v1/sessions/login' => 'sessions#login', :as => 'login'
  get '/api/v1/categories/index'  => 'categories#index', :as => 'category'
  get '/api/v1/products/index' => 'products#index', :as => 'product'
  post '/api/v1/products/search' => 'products#search', :as => 'search'
end
