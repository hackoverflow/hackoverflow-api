class CreateProducts < ActiveRecord::Migration[5.1]
  def up
    create_table :products do |t|
      t.integer :category_id
      t.string :barcode
      t.string :item_name
      t.string :brand
      t.string :description
      t.decimal :standard_retail_price
      t.string :image

      t.timestamps
    end
  end

  def down
    drop_table :products
  end
end
