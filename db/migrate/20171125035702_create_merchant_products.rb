class CreateMerchantProducts < ActiveRecord::Migration[5.1]
  def up
    create_table :merchant_products do |t|
      t.integer :user_id
      t.integer :product_id

      t.timestamps
    end
  end

  def down
  	drop_table :merchant_products
  end
end
