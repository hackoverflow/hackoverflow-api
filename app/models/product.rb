class Product < ApplicationRecord
	belongs_to :category
	has_many :merchant_products
	mount_uploader :image, ImageUploader

	scope :filter, ->(filter) { where(category_id: filter) }
	
	def self.search(search)
		where("item_name LIKE ? OR brand LIKE ?", "%#{search}%", "%#{search}%") 
	end
end
