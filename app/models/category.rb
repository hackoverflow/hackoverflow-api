class Category < ApplicationRecord
	has_many :products
	validates_presence_of :name
	mount_uploader :image, ImageUploader
end
