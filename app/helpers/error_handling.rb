module ErrorHandling

	
	def conflict(msg)
		@http_status = 422
		@http_response = { :error => msg }
	end

	def validation_error(error)
		@http_status = 422
		@http_response = error
	end

	def message(msg)
		@http_status = 200
		@http_response = { :message => msg }
	end

	def obj(model)
		@http_status = 200
		@http_response = model
	end
end
