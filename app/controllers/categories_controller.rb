class CategoriesController < ApplicationController
  include ErrorHandling

  def index
    categories = Category.all()
      @http_response = categories
      @http_status = 200
      render :json => @http_response,:status => @http_status
  end
end
