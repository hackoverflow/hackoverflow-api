class ProductsController < ApplicationController
  include ErrorHandling

  def index
    @products =
    if product_params[:category_id]
      Product.filter(product_params[:category_id])
    else
      @products = Product.all
    end
    @http_response = @products
    @http_status = 200
    render :json => @http_response, :status => @http_status
  end

  def search
    @products = Product.search(params[:search])
    @http_response = @products
    @http_status = 200
    render :json => @http_response, :status => @http_status
  end

  private

  def product_params
    params.permit(:category_id)
  end
end