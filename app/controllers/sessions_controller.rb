class SessionsController < ApplicationController
	include ErrorHandling

	def intialize
		@http_status = 400
    @http_response = {}
	end


	def login
		user = User.find_for_database_authentication(:email => params[:email])
		if user
			if user.valid_password?(params[:password])
				access_token(user)
				@http_response = user.reload
				@http_status = 200
			else
				conflict("Invalid Password")
			end
			
		else
			conflict("Invalid Email")
		end
		render :json => @http_response, :include => [:role], :status => @http_status
	end

	def access_token(user)
		token = SecureRandom.hex(16)
		user.update_attribute(:access_token, token)
		
	end
end
